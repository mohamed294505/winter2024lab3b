import java.util.Scanner;
public class VirtualPetApp {
    public static void main(String[] args) {
		Eagle[] eaglets = new Eagle[4];
		Scanner input = new Scanner(System.in);
		
		for(int i = 0; i < eaglets.length; i++){
			System.out.println("\nPlease enter the breed of eagle " + (i+1));		
			String breed = input.nextLine();
				
			System.out.println("Please enter the speed of the breed in Km/h");		
			int speed = input.nextInt();
			
			System.out.println("Please enter the size of the breed in meters");		
			double size = input.nextDouble();
			input.nextLine();
			
			eaglets[i] = new Eagle(breed, speed, size);
		}
		
		System.out.println("Eagle breed is " + eaglets[eaglets.length-1].breed);		
		System.out.println("Eagle speed is " + eaglets[eaglets.length-1].speed);		
		System.out.println("Eagle size is " + eaglets[eaglets.length-1].size);
		
		eaglets[0].description();
		eaglets[0].topSpeed();

    }
}
